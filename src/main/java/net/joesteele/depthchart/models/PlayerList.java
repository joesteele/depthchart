package net.joesteele.depthchart.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by joesteele on 1/7/15.
 */
public class PlayerList {
  @SerializedName("player")
  public List<Player> players;
}
