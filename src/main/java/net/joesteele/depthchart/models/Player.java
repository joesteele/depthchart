package net.joesteele.depthchart.models;

/**
 * Created by joesteele on 1/7/15.
 */
public class Player {
  public String playerId;
  public String displayName;
  public int depthOrder;
  public String position;
  public String jersey;
}
