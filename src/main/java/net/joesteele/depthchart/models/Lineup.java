package net.joesteele.depthchart.models;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joesteele on 1/7/15.
 */
public class Lineup {
  @SerializedName("id")
  public String typeId;
  @SerializedName("positions")
  public PositionList positionList;

  public static enum Type { Offense, Defense, SpecialTeams }

  private static Map<String, Type> typeMap;
  static {
    typeMap = new HashMap<>();
    typeMap.put("offense", Type.Offense);
    typeMap.put("defense", Type.Defense);
    typeMap.put("specialTeams", Type.SpecialTeams);
  }

  public Type type() {
    return typeMap.get(typeId);
  }

  public String name() {
    switch(type()) {
      case Offense: return "Offense";
      case Defense: return "Defense";
      case SpecialTeams: return "Special Teams";
      default: throw new IllegalStateException("Unknown lineup type");
    }
  }
}
