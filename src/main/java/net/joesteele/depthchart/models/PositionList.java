package net.joesteele.depthchart.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by joesteele on 1/7/15.
 */
public class PositionList {
  @SerializedName("position")
  public List<Position> positions;
}
