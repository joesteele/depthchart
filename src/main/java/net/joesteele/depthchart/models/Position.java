package net.joesteele.depthchart.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by joesteele on 1/7/15.
 */
public class Position {
  public String depthPosition;
  public String displayPosition;
  @SerializedName("players")
  public PlayerList playerList;
}
