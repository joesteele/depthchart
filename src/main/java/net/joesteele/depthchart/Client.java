package net.joesteele.depthchart;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import net.joesteele.depthchart.models.Lineup;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.GET;
import rx.Observable;

import java.util.List;

/**
 * Created by joesteele on 1/7/15.
 */
public class Client {
  private static final String ENDPOINT = "FILL ME IN WITH HOST";
  private static Adept service;

  interface Adept {
    @GET("FILL ME IN WITH ENDPOINT")
    Observable<List<Lineup>> lineups();
  }

  public static Adept get() {
    if (service == null) {
      service = buildAdapter().create(Adept.class);
    }
    return service;
  }

  private static RestAdapter buildAdapter() {
    return new RestAdapter.Builder()
      .setEndpoint(ENDPOINT)
      .setConverter(new GsonConverter(new GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()))
      .build();
  }
}
