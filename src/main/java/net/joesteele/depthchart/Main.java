package net.joesteele.depthchart;

import java.util.stream.Collectors;

/**
 * Created by joesteele on 1/7/15.
 */
public class Main {
  public static void main(String[] args) throws Exception {
    Client.get()
      .lineups()
      .flatMapIterable(list -> list)
      .map(lineup ->
          String.format("%s:\n%s\n", lineup.name(), lineup.positionList.positions.stream()
            .map(position -> String.format("%s: %s", position.displayPosition, position.playerList.players.stream()
              .filter(player -> player.depthOrder == 1)
              .map(player -> player.displayName)
              .collect(Collectors.joining(", "))))
            .collect(Collectors.joining("\n")))
      ).forEach(System.out::println);
  }
}
